package quick_sort;

public class s_qsort {
    
    private int[] data;
    private int left;
    private int right;

    public s_qsort(int[] data) {
        this.data = data;
        left = 0;
        right = data.length - 1;
    }
    
    public void do_sort(){
        quickSort(data,left,right);
    }
    
    private int partition(int arr[], int left, int right){
        int i = left, j = right;
        int tmp;
        int pivot = arr[(left + right) / 2];
        while (i <= j) {
            while (arr[i] < pivot)
                i++;
            while (arr[j] > pivot)
                j--;
            if (i <= j) {
                tmp = arr[i];
                arr[i] = arr[j];
                arr[j] = tmp;
                i++;
                j--;
            }
        }
        return i;
    }

    private void quickSort(int arr[], int left, int right) {
          int index = partition(arr, left, right);
          if (left < index - 1)
                quickSort(arr, left, index - 1);
          if (index < right)
                quickSort(arr, index, right);
    }
    
}
