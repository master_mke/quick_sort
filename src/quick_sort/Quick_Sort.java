package quick_sort;

import java.io.*;
import java.util.Random;
import java.time.Duration;
import java.time.Instant;
import java.util.Arrays;
import java.util.concurrent.ForkJoinPool;

public class Quick_Sort {

    public static void main(String[] args) throws IOException {
        
        int i,r,c;
        int isize,size = 10000;
        int runs=10;
        int ptottime=0;
        int stottime=0;
        int ptime,stime;
        int[] pdata;
        int[] sdata;
        String row;
        int[] header;
        int step=10000;
        int cols=5;
        float[] pavgs;
        float[] savgs;
        s_qsort sqsort;
        
        //loading command line parameters
        if(args.length == 1)
            size=Integer.parseInt(args[0]);
        if(args.length == 2){
            size=Integer.parseInt(args[0]);
            runs=Integer.parseInt(args[1]);
        }
        if(args.length == 3){
            size=Integer.parseInt(args[0]);
            runs=Integer.parseInt(args[1]);
            cols=Integer.parseInt(args[2]);
        }
        if(args.length == 4){
            size=Integer.parseInt(args[0]);
            runs=Integer.parseInt(args[1]);
            cols=Integer.parseInt(args[2]);
            step=Integer.parseInt(args[3]);
        }
        
        header= new int[cols];
        pavgs= new float[cols];
        savgs= new float[cols];

        isize=size;
        
        for(c=0;c<cols;c++){
            ptottime=0;
            stottime=0;
            header[c]=size;
            
            pdata = new int[size];
            sdata = new int[size];
            
            Random rand = new Random();
            
            for(r=0;r<runs;r++){
                System.out.println("Starting parallel run no."+r);
                System.out.println("Filling array for run no."+r);

                //filling the arrays
                for (i = 0; i < size; i++) {
                    pdata[i] = rand.nextInt(size + 1);
                    sdata[i] = pdata[i];
                }
                System.out.println("Arrays filled...");
                System.out.println("Sorting arrays...");

                //parallel run
                p_qsort pquickSort = new p_qsort(pdata);
                ForkJoinPool pool = new ForkJoinPool();
                Instant pstart = Instant.now();
                pool.invoke(pquickSort);
                Instant pend = Instant.now();
                Duration ptimeElapsed = Duration.between(pstart, pend);
                ptime=(int) ptimeElapsed.toMillis();
                pool.shutdown();
                
                //serial run
                s_qsort squickSort = new s_qsort(sdata);
                Instant sstart = Instant.now();
                squickSort.do_sort();
                Instant send = Instant.now();
                Duration stimeElapsed = Duration.between(sstart, send);
                stime=(int) stimeElapsed.toMillis();

                //printing run result and calculate totals
                System.out.println(size + "items arrays sorted...");
                System.out.println("Time taken in parallel: "+ ptime +" milliseconds in run no."+r);
                System.out.println("Time taken in serial: "+ stime +" milliseconds in run no."+r);
                ptottime += ptime;
                stottime += stime;
            }
            //calculate average of this run then printing out the run average and number
            pavgs[c]=ptottime/runs;
            savgs[c]=stottime/runs;
            System.out.println("Average time taken for parallel  "+ runs +" runs = "+ pavgs[c] +" milliseconds");
            System.out.println("Average time taken for serial  "+ runs +" runs = "+ savgs[c] +" milliseconds");
            size=size+step;
        }
        //printing out run data 
        System.out.println("\r\nArray size and average time taken for each size");
        System.out.println("Number of runs:"+runs);
        System.out.println("Number of columns:"+cols);
        System.out.println("value for first column:"+isize);
        System.out.println("Column Step:"+step);
        
        //creating CSV file 
        BufferedWriter output = null;
        try {
            File file = new File("results.csv");
            output = new BufferedWriter(new FileWriter(file));
            row=Arrays.toString(header);
            System.out.println("Size ,"+row.substring(1,row.lastIndexOf("]")));
            output.write("Size ,"+row.substring(1,row.lastIndexOf("]"))+"\r\n");
            row=Arrays.toString(pavgs);
            System.out.println("Parallel,"+row.substring(1,row.lastIndexOf("]")));
            output.write("Parallel,"+row.substring(1,row.lastIndexOf("]"))+"\r\n");
            row=Arrays.toString(savgs);
            System.out.println("Serial,"+row.substring(1,row.lastIndexOf("]")));
            output.write("Serial,"+row.substring(1,row.lastIndexOf("]"))+"\r\n");
        }catch ( IOException e ) {
            System.out.println("File write error!...\r\n"+e.getMessage());
        }finally{
            if ( output != null ) {
              output.close();
            }
        }
    }
}
